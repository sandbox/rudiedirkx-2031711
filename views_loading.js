
(function($) {

	var $window = $(window);

	function extendAfter(container, funcName, newCallback, overrideRetVal) {
		var oldCallback = container[funcName];
		container[funcName] = function() {
			var oldRetVal = oldCallback.apply(this, arguments);
			var newRetVal = newCallback.apply(this, arguments);
			return overrideRetVal ? newRetVal : oldRetVal;
		};
	};

	Drupal.behaviors.views_loading = {
		attach: function(context, settings, go) {
			// This hideous beast is to ensure we're the very last behaviour to trigger in this sequence.
			if ( !go ) {
				return setTimeout(function() {
					Drupal.behaviors.views_loading.attach(context, settings, 1);
				}, 1);
			}

			// Add global pager event triggers to all Views pager elements.
			if ( Drupal.views && Drupal.views.instances ) {
				$.each(Drupal.views.instances, function(elName, options) {
					// Because of a Views bug, only the last Drupal.ajax object is stored, so
					// only the last link in the pager can have below ajax handlers =(
					// @see https://drupal.org/node/2051869
					// This is why Views has to be patched (see README.txt) and why options.links
					// exists
					if ( options.links && options.links.length ) {
						options.links.forEach(function(ajax) {
							// Prepare custom event with custom data. We can reuse this event for
							// both triggers.
							var e = new jQuery.Event('x');
							e.options = options;
							e.ajax = ajax;

							// Custom event BEFORE ajax.
							if ( ajax.options.beforeSend ) {
								extendAfter(ajax.options, 'beforeSend', function() {
									e.type = 'afterBeforeSendViewsPager';
									$window.trigger(e);
								});
							}

							// Custom event AFTER ajax.
							if ( ajax.options.complete ) {
								extendAfter(ajax.options, 'complete', function() {
									e.type = 'afterCompleteViewsPager';
									$window.trigger(e);
								});
							}
						});
					}
				});
			}
		}
	};

	// Implement custom event: BEFORE ajax.
	$window.bind('afterBeforeSendViewsPager', function(e) {
		e.options.$view.addClass('loading');
	});

	// Implement custom event: AFTER ajax.
	$window.bind('afterCompleteViewsPager', function(e) {
		e.options.$view.removeClass('loading');
	});

})(jQuery);
